import urllib,urllib2,re,xbmcplugin,xbmcgui,xbmcaddon

def CATEGORIES():
        addDir('Leagues','leagues',None,'leagues_thumb.jpg','leagues.jpg')
        addDir( 'Cups','cups',None,'cups_thumb.jpg','cups.jpg')
        addDir('Full Matches','http://lasthighlight.com/category/full-match/',1,'fullmatch_thumb.jpg','fullmatch.jpg')
        addDir('Shows','shows',None,'shows_thumb.jpg','shows.jpg')
        addDir( 'Internationals','international',None,'','')
        addDir( 'Other','http://lasthighlight.com/category/others/',1,'','')
        addDir('Search...','search',3,'search_thumb.png','')
        #Addon.getLocalizedString(32000)

def LEAGUES():
        addDir('Premier League','http://lasthighlight.com/category/premier-league/',1,'premierleague_thumb.jpg','premierleague.jpg')
        addDir('Liga BBVA','http://lasthighlight.com/category/la-liga/',1,'laliga_thumb.png','laliga.jpg')
        addDir('Serie A','http://lasthighlight.com/category/serie-a/',1,'seriea_thumb.jpg','seriea.jpg')
        addDir('Bundesliga','http://lasthighlight.com/category/bundesliga/',1,'bundesliga_thumb.png','bundesliga.jpg')
        addDir('Ligue 1','http://lasthighlight.com/category/others/ligue-1/',1,'ligue1_thumb.jpg','ligue1.jpg')

def CUPS():
        addDir('UEFA Champions League','http://lasthighlight.com/category/champions-league/',1,'championsleague_thumb.png','championsleague.jpg')
        addDir('UEFA Europa League','http://lasthighlight.com/category/europa-league/',1,'europaleague_thumb.png','europaleague.jpg')
        addDir('AFC Champions League','http://lasthighlight.com/category/afc-champions-league/',1,'afc_thumb.png','afc.jpg')
        addDir( 'Capital One Cup','http://lasthighlight.com/category/capital-one-cup/',1,'capitalone_thumb.jpg','capitalone.jpg')
        addDir( 'FA Cup','http://lasthighlight.com/category/fa-cup/',1,'facup_thumb.jpg','facup.jpg')
        addDir( 'Copa Del Rey','http://lasthighlight.com/category/la-liga/copa-del-rey/',1,'','')
        addDir( 'Spanish Super Cup','http://lasthighlight.com/category/la-liga/super-cup/',1,'','')
        addDir( 'DFB Pokal','http://lasthighlight.com/category/bundesliga/dfb-pokal/',1,'','')
        addDir( 'Coppa Italia','http://lasthighlight.com/category/serie-a/coppa-italia/',1,'','')
        addDir( 'UEFA Super Cup','http://lasthighlight.com/category/champions-league/european-super-cup/',1,'','')

def INTERNATIONAL():
        addDir( 'All Internationals','http://lasthighlight.com/category/international/',1,'','')
        addDir('UEFA European Championship','http://lasthighlight.com/category/international/euro/',1,'euro_thumb.png','euro.jpg')
        addDir( 'FIFA Under-20 World Cup','http://lasthighlight.com/category/international/fifa-u-20-world-cup-2015/',1,'','')
        addDir( 'UEFA Under-21 European Championship','http://lasthighlight.com/category/euro-cup-u21-2015/',1,'','')
        addDir( '2015 Gold Cup','http://lasthighlight.com/category/international/gold-cup-2015/',1,'','')
        addDir( 'Africa Cup of Nations','http://lasthighlight.com/category/international/caf-africa-cup-of-nations/',1,'','')
        addDir( 'Copa America','http://lasthighlight.com/category/international/copa-america-2015/',1,'','')
        addDir( 'International Friendlies','http://lasthighlight.com/category/international/world-friendlies/',1,'','')

def SHOWS():
        addDir( 'Match Of The Day','http://lasthighlight.com/category/premier-league/motd/#',1,'motd_thumb.jpg','motd.jpg')
        addDir( 'Premier League Review','http://lasthighlight.com/category/shows/premier-league-review/',1,'premierleague_thumb.jpg','premierleague.jpg')
        addDir( 'Serie A Review','http://lasthighlight.com/category/shows/serie-a-review/',1,'seriea_thumb.jpg','seriea.jpg')
        addDir( 'Bundesliga Review','http://lasthighlight.com/category/shows/bundesliga-review/',1,'bundesliga_thumb.png','bundesliga.jpg')
        addDir( 'Liga BBVA Review','http://lasthighlight.com/category/shows/liga-bbva-review/',1,'laliga_thumb.png','laliga.jpg')
        addDir( 'Monday Night Football','http://lasthighlight.com/category/shows/monday-night-football/',1,'mnf_thumb.png','mnf.jpg')

def SEARCH():
        query=xbmcgui.Dialog().input('Search...',type=xbmcgui.INPUT_ALPHANUM)
        if query:
                url='http://lasthighlight.com/?s='+query
                INDEX_SEARCH(url)

def INDEX_SEARCH(url):
        link=get_url(url)
        main_content = get_main_content(link)
        match=re.compile('<div class="td-module-thumb"><a href="(.+?)" rel="bookmark" title="(.+?)"><img width="" height="" itemprop="image" class="entry-thumb" src="(.+?)"').findall(main_content)
        for url,name,thumbnail in match:
                addDir(name,url,2,thumbnail,'')
        match_next=re.search('<a href="(.+?)" ><i class="td-icon-menu-right">', link)
        if match_next:
                INDEX_SEARCH(match_next.group(1))

def INDEX(url):
        link=get_url(url)
        main_content = get_main_content(link)
        match=re.compile('<div class="td-module-thumb"><a href="(.+?)" rel="bookmark" title="(.+?)"><img width="" height="" itemprop="image" class="entry-thumb" src="(.+?)"').findall(main_content)
        for url,name,thumbnail in match:
                addDir(name,url,2,thumbnail,'')
        match_next=re.search('<a href="(.+?)" ><i class="td-icon-menu-right">', link)
        if match_next:
                addDir('Next page...',match_next.group(1),1,'','')

def get_main_content(link):
        content, footer = link.split('td-footer-wrapper')
        if not content: content = link
        main_content, sidebar = content.split('td-main-sidebar')
        if not main_content: main_content = content
        return main_content
				
def VIDEOLINKS(url,name):
        link=get_url(url)
        match_image=re.search('<img src="(.+?)".+data-recalc-dims',link)
        thumbnail=''
        if match_image:
                thumbnail=match_image.group(1)
        match_tabs=re.search('<ul class="nav nav-tabs oscitas-bootstrap-container" id="oscitas-tabs-0">',link)
        if match_tabs:
                match=re.compile('href="#(.+?)" data-toggle="tab">(.+?)</a>').findall(link)
                for id,name in match:
                        match_link=re.search('(?:id="'+id+'">.+"file":"|<source src=")(.+?)"', link)
                        if match_link:
                                addLink(name,match_link.group(1),thumbnail)
        else:
                match_title=re.search('<h1 (?:itemprop="name" )*class="entry-title">(.+?)</h1>', link)
                if match_title:
                        name=match_title.group(1)
                        #match_link=re.search('.+"file":"(.+?)"', link)
                        match_link=re.search('(?:.+"file":"|<source src=")(.+?)"', link)
                        if match_link:
                                addLink(name,match_link.group(1),thumbnail)

def get_url(url):
        req = urllib2.Request(url)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
        response = urllib2.urlopen(req)
        link=response.read()
        response.close()
        return link.replace('&#038;','&').replace('&#8211;','-')
		
def get_params():
        param=[]
        paramstring=sys.argv[2]
        if len(paramstring)>=2:
                params=sys.argv[2]
                cleanedparams=params.replace('?','')
                if (params[len(params)-1]=='/'):
                        params=params[0:len(params)-2]
                pairsofparams=cleanedparams.split('&')
                param={}
                for i in range(len(pairsofparams)):
                        splitparams={}
                        splitparams=pairsofparams[i].split('=')
                        if (len(splitparams))==2:
                                param[splitparams[0]]=splitparams[1]
                                
        return param

def addLink(name,url,iconimage):
        ok=True
        liz=xbmcgui.ListItem(name, iconImage="DefaultVideo.png", thumbnailImage=iconimage)
        liz.setInfo( type="Video", infoLabels={ "Title": name } )
        ok=xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]),url=url,listitem=liz)
        return ok


def addDir(name,url,mode,iconimage,fanart):
        if iconimage and not iconimage.startswith('http://'):
                iconimage=MediaPath+iconimage
        if fanart and not fanart.startswith('http://'):
                fanart=MediaPath+fanart
        u=sys.argv[0]+"?url="+urllib.quote_plus(url)+"&mode="+str(mode)+"&name="+urllib.quote_plus(name)
        ok=True
        liz=xbmcgui.ListItem(name, iconImage="DefaultFolder.png", thumbnailImage=iconimage)
        liz.setInfo( type="Video", infoLabels={ "Title": name } )
        if fanart:
                liz.setArt( { 'fanart': fanart } )
        ok=xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]),url=u,listitem=liz,isFolder=True)
        return ok
        
Addon = xbmcaddon.Addon()
Path = Addon.getAddonInfo('path')
MediaPath = Path + '/resources/media/'

params=get_params()
url=None
name=None
mode=None

try:
        url=urllib.unquote_plus(params["url"])
except:
        pass
try:
        name=urllib.unquote_plus(params["name"])
except:
        pass
try:
        mode=int(params["mode"])
except:
        pass

print "Mode: "+str(mode)
print "URL: "+str(url)
print "Name: "+str(name)

if mode==None or url==None or len(url)<1:
        print ""
        if url=='leagues':
                LEAGUES()
        elif url=='shows':
                SHOWS()
        elif url=='cups':
                CUPS()
        elif url=='international':
                INTERNATIONAL()
        else:
                CATEGORIES()
       
elif mode==1:
        print ""+url
        INDEX(url)
        
elif mode==2:
        print ""+url
        VIDEOLINKS(url,name)
        
elif mode==3:
        print ""+url
        SEARCH()
        
elif mode==4:
        print ""+url
        INDEX_SEARCH(url)

xbmcplugin.endOfDirectory(int(sys.argv[1]))